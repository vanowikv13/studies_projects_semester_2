def eukulides(a, b):
    while(b != 0):
        r = a % b
        a = b
        b = r
    return a

def eukulides_min(a, b):
    while b != a:
        if(b > a):
            b = b - a
        else:
            a = a - b
    return b

def fib(n):
    if(n == 2 or n == 1):
        return 1
    else: return fib(n - 2) + fib(n - 1);
