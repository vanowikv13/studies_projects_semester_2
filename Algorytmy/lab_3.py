def search_rec(arr, l, r, x):
    if r >= l:
        mid = (l + r) // 2
        if arr[mid] == x: 
            return mid 
        elif arr[mid] > x: 
            return search_rec(arr, l, mid-1, x)
        else: 
            return search_rec(arr, mid+1, r, x)
  
    else:
        return -1

def search(arr, l, r, x):
    if x < arr[0] or x > arr[r]:
        return -1
    elif x == arr[0]:
        return 0
    elif x == arr[r]:
        return r
    while l <= r:
        mid = (l + r) // 2
        if arr[mid] == x:
            return mid
        elif arr[mid] < x:
            l = mid + 1
        else:
            r = mid - 1
    return -1

arr = [ 2, 3, 4, 10, 40] 
index = search(arr, 0, len(arr) - 1, 4)
print(index + 1)
