def pow(a, k):
    if(k == 0):
        return 1
    return a**(k % 2) * pow(a * a, k // 2)

# ** - potęga
# "//" - dzielenie całkowite

def pow_it(a, k):
    b = a
    p = 1
    if(k % 2 == 0):
        p = a
        k = k//2
    while(k != 0):
        b = b*b
        if(k % 2 == 1):
            p = p * b
        k = k // 2
    return p

print(pow_it(2, 4))
print(pow(2, 4))

def pow_req(a, k):
    if k == 0:
        return 1
    if k % 2 == 0:
        x = pow_req(a, k//2)
        return x * x
    x = pow_req(a, k//2)
    return a * x * x

print(pow_req(2, 5))
