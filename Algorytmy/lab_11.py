
#Merge Sort 
def mergeSort(arr): 
    if len(arr) >1: 
        mid = len(arr)//2 #Finding the mid of the array 
        L = arr[:mid] # Dividing the array elements  
        R = arr[mid:] # into 2 halves 
  
        mergeSort(L) # Sorting the first half 
        mergeSort(R) # Sorting the second half 
  
        i = j = k = 0
          
        # Copy data to temp arrays L[] and R[] 
        while i < len(L) and j < len(R): 
            if L[i] < R[j]: 
                arr[k] = L[i] 
                i+=1
            else: 
                arr[k] = R[j] 
                j+=1
            k+=1
          
        # Checking if any element was left 
        while i < len(L): 
            arr[k] = L[i] 
            i+=1
            k+=1
          
        while j < len(R): 
            arr[k] = R[j] 
            j+=1
            k+=1

if __name__ == '__main__': 
    arr = [12, 11, 13, 5, 6, 7]  
    print(arr)
    mergeSort(arr) 
    print(arr)



"""
T(N) = a*T(N/b) + f(N)
a = 2 b = 2
f(N) = Theta(N)
logba = log2 od 2 = 1
N1 = N
T(N) = Theta(N log N




T(N) = 2*T(N/2)+N^2
a = 2 b = 2
f(N) = Theta(N^(1+e))
labba = log 2 od 2 = 1
N1 = N
T(N) = Theta(n^2)
"""<