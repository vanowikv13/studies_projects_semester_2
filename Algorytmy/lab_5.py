# algorytm rekurencyjny silni

def silnia(n):
    if n == 1:
        return 1
    else:
        return n * silnia(n - 1)
    
print(silnia(4))

"""
Pseudocode  
factorial(n)
{
    if(n = 1) return(1);
    return (factorial(n-1)*n);
    
}
"""

def piramid(n):
    if n == 1:
        return 1
    else:
        return n*n + piramid(n -1)
    
    
def piramid_it(n):
    t = 0;
    for i in range(n + 1):
        t = t + (i * i)
    return t

print(piramid(5))
print(piramid_it(5))


"""
piramid(n)
{
    if(n = 1)
        return 1;
    else
        return n * n + piramid(n - 1)
}

piramid_it(n)
{
    t <- 1;
    for(i <- 2; i <= n; i++) {
        t += i * i;
    }
    return t;

}
"""


"""
horner(a[], x, n) {
    t <- 0;
    for(i <- 1; i < n - 1; i++) {
        t += a[i] * x + a[i + 1];
    }
    return t;
}


horner(a[], x, n) {
    if(x = 0)
        return a[0];
    return n * horner(a, x - 1, n) + a[x];
    
}
"""

def horner(a, x, n):
    t = 0
    for i in range(n - 1):
        t = t + a[i] * x + a[i + 1]
    return t
print(horner([1, 1, 1], 2, 3))
       
       
# dla n >= 1
def potega(x, n):
    if(n == 1):
        return x;
    elif(n % 2 == 0):
        return potega(x, n//2) * potega(x, n//2)
    else:
        return potega(x, n//2) * potega(x, n//2) * x


print(potega(2, 23232322))


potega_1(x, n) {
    t = 1
    while(n > 0) {
        if(n%2 == 1)
            t*=x;
        x*=x;
        n/=2;
    }
    return t;
}
    
    
power_it(a, k) {
b <- a;
p <- 1;
if(k mod 2 == 1) p <- a;
k <- k/2;

}

