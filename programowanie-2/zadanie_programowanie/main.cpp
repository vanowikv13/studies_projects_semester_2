#include <iostream>
#include "implementation/songs_databse.h"
#include "interfaces/music_player_app_controller.h"
#include "implementation/music_player.h"
#include "implementation/music_player_app.h"
#include "interfaces/song_interface.h"
#include "interfaces/database_controller.h"
#include "implementation/super_music_player.h"
#include "implementation/song.h"

using namespace std;


//saving from player to CSV file
//[11/2] - operator overload <<
MusicPlayerApp& operator<<(fstream& file, MusicPlayerApp&app) {
    auto db = app.getDB();
    db->setStream(file);
    db->write(db->getData());
    return app;
}

//saving from CSV file to player
//[11/2] operator overload >>
MusicPlayerApp& operator>>(fstream &file, MusicPlayerApp&app) {
    auto db = app.getDB();
    db->setStream(file);
    db->read();
}


int main()
{
    //[12/1] - MusicPlayer is our class K that we will store some of it value inside a file
    //the class SongsDatabase is class that manage storing values for this class

    //[11/3] - creatring object K
    MusicPlayerApp *player = new MusicPlayerApp();

    //fstream file to use
    fstream fileHandler;
    fileHandler.open("songs_list.csv", ios::in | ios::out);
    
    //[11/4]
    fileHandler >> (*player); //copy from CSV file to player
    player->writeSongsList(); //writing songs from K

    //[11/5] - adding new song to object K and then write it to file
    player->insert_new_song(new Song("x","y",1,2)); //inserting new object
    fileHandler << (*player); //copy from player to CSV file

    //AGAIN READING FROM FILE TO CHECK IF NEW SONGS WERE ADD
    cout << endl<<endl << "after insert new value: "<<endl;
    fileHandler >> (*player); //copy from CSV file to player
    player->writeSongsList(); //writing songs from K

    fileHandler.close();
    return 0;
}
