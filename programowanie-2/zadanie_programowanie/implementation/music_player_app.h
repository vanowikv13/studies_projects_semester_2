#ifndef MUSIC_PLAYER_APP_H_INCLUDED
#define MUSIC_PLAYER_APP_H_INCLUDED

#include "../interfaces/player.h"
#include "../interfaces/speaker_controller.h"
#include "speaker.h"
#include "music_player.h"
#include "../interfaces/music_player_app_controller.h"
#include "songs_databse.h"
#include "../interfaces/song_interface.h" //uinsg freind writeSongInfo


#include <iostream>
#include <iomanip>

void writeSongInfo(SongBase *song, int actailMinute, int actualSecond)
{
    if (song != nullptr)
    {
        std::cout << "Name: " << song->getName() << std::endl;
        std::cout << "Author: " << song->getAuthor() << std::endl;
        std::cout << "Time: " << song->getMinutes() << " : " << song->getSeconds() << " | " << actailMinute << ":" << actualSecond << std::endl;
    }
    else
    {
        std::cout << "song is not played" << std::endl;
    }
}

class MusicPlayerApp : public MusicPlayerAppController
{
public:
    MusicPlayerApp(DatabaseController<SongBase*> *db = new SongsDatabase<SongBase*>(), Player *p = new MusicPlayer(), SpeakerController *sc = new Speaker()) : MusicPlayerAppController(p, sc, db) {}

    virtual void mainLoop()
    {
        //this->player->setActualSong(this->dbController->getData()[0]);
        //this->player->setSongList(this->dbController->getData());
        //menu();
    }

    void writeSongsList() {
        for(auto x : this->dbController->getData()) {
            writeSongInfo(x, 0,0);
        }
    }

    void insert_new_song(SongBase*b) {
        dbController->getData().push_back(b);
        player->setSongList(dbController->getData());
    }

    void menu()
    {
        //std::cout << "Music player" << std::endl;
        //writeSongInfo(this->player->getActualSong(), this->player->getMinutes(), this->player->getSeconds());
    }
};

#endif // MUSIC_PLAYER_APP_H_INCLUDED
