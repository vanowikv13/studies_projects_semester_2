#ifndef SPEAKER_H_INCLUDED
#define SPEAKER_H_INCLUDED

#include "../interfaces/speaker_controller.h"

class Speaker : public SpeakerController
{
public:
    Speaker() {}

    void increaseVolume()
    {
        if (this->volume < this->maxVolume)
            this->volume++;
    }

    void decreaseVolume()
    {
        if (this->volume > this->minVolume)
            this->volume--;
    }

    unsigned int getVolume()
    {
        return this->volume;
    }
};

#endif // SPEAKER_H_INCLUDED
