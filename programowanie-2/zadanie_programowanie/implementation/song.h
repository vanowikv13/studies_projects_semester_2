#ifndef SONG_H_INCLUDED
#define SONG_H_INCLUDED

#include "../interfaces/song_interface.h"
#include <string>

//[11/1] - class A
class Song : public SongBase
{
public:
    Song(string author, string name, int minutes, int seconds): SongBase(author, name, minutes, seconds) {}
    Song(const Song &song) : SongBase(song.author, song.name, song.minutes, song.seconds){}
    Song(const Song * song) : SongBase(song->author, song->name, song->minutes, song->seconds){}
    void setAuthor(string a)
    {
        this->author = a;
    }

    void setName(string n)
    {
        this->name = n;
    }

    void setMinutes(int m)
    {
        this->minutes = m;
    }

    void setSeconds(int s)
    {
        this->seconds = s;
    }

    string getAuthor()
    {
        return this->author;
    }

    string getName()
    {
        return this->name;
    }

    int getMinutes()
    {
        return this->minutes;
    }

    int getSeconds()
    {
        return this->seconds;
    }
};

#endif // SONG_H_INCLUDED
