#ifndef SUPER_MUSIC_PLAYER_H_INCLUDED
#define SUPER_MUSIC_PLAYER_H_INCLUDED

#include "../interfaces/player.h"
#include "../implementation/song.h"

//[11/1] - B class
class SuperMusicPlayer : public Player
{
public:
    SuperMusicPlayer(SongBase* actualSong = nullptr, vector<SongBase*> actualList = vector<SongBase*>()) : Player(actualSong) {

    }

    //[11/2] - constructor of class B
    SuperMusicPlayer(Song * s) : Player(s) {
        cout << "construcot SuperMusicPlayer(Song * s)" << endl;
        this->actualSong = s;
        this->actualList = vector<SongBase*>({s});
        //using getter
        this->actualSongMinute = s->getMinutes();
        this->actualSongSecond = s->getSeconds();
    }

    //[11/4] - conversion operator operator A()
    operator Song(){
        cout << "operator Song()" << endl;
        Song s("AuthorO", "NameO", this->actualSongMinute + 100, this->actualSongSecond+100);
        return s;
    }

    void start();
    void stop();
    void next();
    void previous();
    void changeLoopStatus();
    bool getLoopStatus();
    SongBase* getActualSong();
    void setActualSong(SongBase* song);
    void setSongList(vector<SongBase*> songList);
    vector<SongBase*> getSongList();
    int getMinutes();
    int getSeconds();
    void setMinute(int m);
    void setSeconds(int s);
};

#endif // SUPER_MUSIC_PLAYER_H_INCLUDED
