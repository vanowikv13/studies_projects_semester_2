#ifndef SONGS_DATABASE_H_INCLUDED
#define SONGS_DATABASE_H_INCLUDED

#include <fstream>
#include <vector>
#include <iostream>
#include <algorithm>
#include <string>

#include "song.h"
#include "../interfaces/database_controller.h"
#include "../interfaces/song_interface.h"

template<class T>
class SongsDatabase : public DatabaseController<T>
{
private:
    //songs will be usually vector<SongBase*>
    std::vector<T> songs;
    fstream file;
    const string fileName;
public:
    std::vector<T> &getData() {
        return songs;
    }

    //[11/6] - reading from CSV
    void read() {
        songs.clear();
        std::string title, author;
        std::string endSign;
        std::string m, s;
        unsigned int minutes, seconds;
        if(file.is_open()) {
            while (getline(file,title,','))
            {
                std::getline(file, author, ',');

                std::getline(file, m, ',');
                std::getline(file, s);
                minutes = std::stoi(m);
                seconds = std::stoi(s);
                songs.push_back(new Song(author, title, minutes, seconds));
            }
        } else {
            std::cout << "file do not were open" << std::endl;
        }

    }

    void setStream(fstream &stream) {
        //need this way cause operator= is deleted for fstream
        if(this->file.is_open())
            this->file.close();
        this->file = std::move(stream);
        if(!file.is_open())
            this->setController();
    }

    //[11/7] - writing to CSV
    void write(std::vector<T> listToWrite) {
        if(listToWrite.size() > 0) {
            for(auto *x : listToWrite) {
                std::string line;
                line = x->getName()+","+x->getAuthor() +","+to_string(x->getMinutes())+","+to_string(x->getSeconds())+'\n';
                file << line;
            }
        }
    }

    void setController() {
        if(this->file.is_open())
            this->file.close();
        this->file.open(this->fileName, ios::in | ios::out);
    }

    SongsDatabase(std::string fileName = "songs_list.csv") : fileName(fileName) {
    }

    virtual ~SongsDatabase() {
        if(this->file.is_open())
            this->file.close();
        this->songs.clear();
    }
};

#endif // SONGS_DATABASE_H_INCLUDED
