#ifndef MUSIC_PLAYER_H_INCLUDED
#define MUSIC_PLAYER_H_INCLUDED

#include "../interfaces/player.h"
#include <fstream>

class MusicPlayer : public Player  {
public:

    MusicPlayer(SongBase* actualSong = nullptr, vector<SongBase*> actualList = vector<SongBase*>())
    : Player(actualSong) {}

    void start();
    void stop();
    void next();
    void previous();
    void changeLoopStatus();
    bool getLoopStatus();
    SongBase* getActualSong();
    void setActualSong(SongBase* song);
    void setSongList(vector<SongBase*> songList);
    vector<SongBase*> getSongList();
    int getMinutes();
    int getSeconds();
    void setMinute(int m);
    void setSeconds(int s);

    //getInfo //ther is definition in partent method
    void writeActualSongInfo(SongBase* song = nullptr);
};

#endif // MUSIC_PLAYER_H_INCLUDED