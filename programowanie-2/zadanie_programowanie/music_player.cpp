#include <string>
#include "implementation/music_player.h"
#include <algorithm>
#include <iostream>
#include <cstring>

void MusicPlayer::start()
{
    this->isMusicOn = !this->isMusicOn;
}

void MusicPlayer::stop()
{
    this->isMusicOn = !this->isMusicOn;
}

void MusicPlayer::next()
{
    if (this->actualList.size() == 0)
        return;
    auto song = find(this->actualList.begin(), this->actualList.end(), this->Player::actualSong);
    if (song == actualList.end())
    {
        this->Player::actualSong = this->actualList[0];
    }
    else
    {
        this->Player::actualSong = *(song + 1);
    }
}

void MusicPlayer::previous()
{
    auto song = find(this->actualList.begin(), this->actualList.end(), this->Player::actualSong);
    int index = song - this->actualList.begin() - 1;
    if (index != 0)
    {
        this->Player::actualSong = this->actualList[index - 1];
    }
    else
    {
        this->Player::actualSong = this->actualList[this->actualList.size() - 1];
    }
}

void MusicPlayer::changeLoopStatus()
{
    this->isLoop = !this->isLoop;
}

bool MusicPlayer::getLoopStatus()
{
    return this->isLoop;
}



void MusicPlayer::setActualSong(SongBase *song)
{
    this->Player::actualSong = song;
}

void MusicPlayer::setSongList(vector<SongBase *> songList)
{
    this->actualList = songList;
}

vector<SongBase *> MusicPlayer::getSongList()
{
    return this->actualList;
}

int MusicPlayer::getMinutes()
{
    return this->actualSongMinute;
}
int MusicPlayer::getSeconds()
{
    return this->actualSongSecond;
}

void MusicPlayer::setMinute(int minute)
{
    this->actualSongMinute = minute;
}
void MusicPlayer::setSeconds(int second)
{
    this->actualSongSecond = second;
}

//getSongInfo //this method overide version of abstract class
void MusicPlayer::writeActualSongInfo(SongBase* song)
{
    std::cout << std::endl;
    std::cout << "from inherited class " << std::endl;
    if(song == nullptr)
        song = this->getActualSong();
    if (song != nullptr)
    {
        std::cout << "Name: " << song->getName() << std::endl;
        std::cout << "Author" << song->getAuthor() << std::endl;
        std::cout << "Time:" << song->getMinutes() << ":" << song->getSeconds() << " | " << actualSongMinute << ":" << actualSongSecond << std::endl;
    }
    else
        std::cout << "song is not played" << std::endl;
}

//9/4
SongBase* MusicPlayer::getActualSong() {
    return this->Player::getActualSong();
}
