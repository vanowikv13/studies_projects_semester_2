#ifndef SPEAKER_CONTROLLER_H_INCLUDED
#define SPEAKER_CONTROLLER_H_INCLUDED

class SpeakerController
{
protected:
    unsigned int volume = 0;
    const unsigned int maxVolume = 100;
    const unsigned int minVolume = 0;
public:
    virtual void increaseVolume() = 0;

    virtual void decreaseVolume() = 0;

    virtual unsigned int getVolume() = 0;
};

#endif // SPEAKER_CONTROLLER_H_INCLUDED
