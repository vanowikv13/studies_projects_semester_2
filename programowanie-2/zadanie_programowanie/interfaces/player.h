#ifndef PLAYER_H_INCLUDED
#define PLAYER_H_INCLUDED

#include "../implementation/song.h"
#include <vector>
#include <iostream>

using namespace std;

//[10/2] - base class
class Player {
protected:
    bool isLoop = false;
    bool isMusicOn = false;
    int actualSongMinute = 0;
    int actualSongSecond = 0;
    SongBase* actualSong;
    std::vector<SongBase*> actualList;

public:
    Player(SongBase* actual) : actualSong(actual) {}
    Player() {}

    virtual void start() = 0;
    virtual void stop() = 0;
    virtual void next() = 0;
    virtual void previous() = 0;
    virtual void changeLoopStatus() = 0;
    virtual bool getLoopStatus() = 0;

    virtual void setActualSong(SongBase* song) = 0;
    virtual void setSongList(vector<SongBase*> songList) = 0;
    virtual vector<SongBase*> getSongList() = 0;
    virtual int getMinutes() = 0;
    virtual int getSeconds() = 0;
    virtual void setMinute(int) = 0;
    virtual void setSeconds(int) = 0;

    virtual SongBase* getActualSong() {
        return this->actualSong;
    }


    //[10/2] - method x
    virtual void writeActualSongInfo(SongBase* song = nullptr)
    {
        std::cout << "from abstract class " << std::endl;
        if(song == nullptr)
            song = this->getActualSong();
        if (song != nullptr)
        {
            std::cout << "Name: " << song->getName() << std::endl;
            std::cout << "Author" << song->getAuthor() << std::endl;
            std::cout << "Time:" << song->getMinutes() << ":" << song->getSeconds() << " | " << actualSongMinute << ":" << actualSongSecond << std::endl;
        }
        else
        {
            std::cout << "song is not played" << std::endl;
        }
    }
};

#endif // PLAYER_H_INCLUDED
