#ifndef DATABASE_CONTROLLER_H_INCLUDED
#define DATABASE_CONTROLLER_H_INCLUDED
#include <vector>
#include <fstream>

template<class T>
class DatabaseController
{
public:
    virtual std::vector<T> &getData() = 0;
    virtual void setController() = 0;
    virtual void write(std::vector<T>) = 0;
    virtual void read() = 0;
    virtual void setStream(fstream &stream) = 0;
};

#endif // DATABASE_CONTROLLER_H_INCLUDED
