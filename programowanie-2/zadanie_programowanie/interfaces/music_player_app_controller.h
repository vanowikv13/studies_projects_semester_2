#ifndef MUSIC_PLAYER_APP_CONTROLLER_H_INCLUDED
#define MUSIC_PLAYER_APP_CONTROLLER_H_INCLUDED

#include "player.h"
#include "speaker_controller.h"
#include "database_controller.h"

class MusicPlayerAppController {
protected:
    Player * player;
    SpeakerController * speakerController;
    DatabaseController<SongBase*> * dbController;
public:
    MusicPlayerAppController(Player *p, SpeakerController * sc, DatabaseController<SongBase*> *db) {
        this->player = p;
        this->speakerController = sc;
        this->dbController = db;
    }

    virtual void mainLoop() = 0;

    //to delete
    Player* getPlayer() {
        return this->player;
    }

    DatabaseController<SongBase*>* getDB(){
        return this->dbController;
    }

    ~MusicPlayerAppController() {
        delete this->player;
        delete this->speakerController;
        delete this->dbController;
    }
};

#endif // MUSIC_PLAYER_APP_CONTROLLER_H_INCLUDED