#ifndef SONG_INTERFACE_H_INCLUDED
#define SONG_INTERFACE_H_INCLUDED

#include <string>
#include <iostream>

using namespace std;

class SongBase {
protected:
    string author;
    string name;
    int minutes;
    int seconds;
public:
    SongBase(string author, string name, int minutes, int seconds) : author(author), name(name), minutes(minutes), seconds(seconds) {}
    SongBase(const SongBase * song) {
        author = song->author;
        name = song->name;
        minutes = song->minutes;
        seconds = song->seconds;
    }

    SongBase(const SongBase & song) : SongBase(&song) {}

    virtual void setAuthor(string a) = 0;

    virtual void setName(string n) = 0;

    virtual void setMinutes(int m) = 0;

    virtual void setSeconds(int s) = 0;

    virtual string getAuthor() = 0;

    virtual string getName() = 0;

    virtual int getMinutes() = 0;

    virtual int getSeconds() = 0;
};

#endif // SONG_INTERFACE_H_INCLUDED
