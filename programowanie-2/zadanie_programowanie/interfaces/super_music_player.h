#ifndef SUPER_MUSIC_PLAYER_H_INCLUDED
#define SUPER_MUSIC_PLAYER_H_INCLUDED

#include "player.h"

//[10/1] - p2
class SuperMusicPlayer : public Player
{
public:
    SuperMusicPlayer(SongBase* actualSong = nullptr, vector<SongBase*> actualList = vector<SongBase*>()) : Player(actualSong) {

    }
    ~SuperMusicPlayer() {}

    void start();
    void stop();
    void next();
    void previous();
    void changeLoopStatus();
    bool getLoopStatus();
    SongBase* getActualSong();
    void setActualSong(SongBase* song);
    void setSongList(vector<SongBase*> songList);
    vector<SongBase*> getSongList();
    int getMinutes();
    int getSeconds();
    void setMinute(int m);
    void setSeconds(int s);

    void writeActualSongInfo();
};

#endif // SUPER_MUSIC_PLAYER_H_INCLUDED
