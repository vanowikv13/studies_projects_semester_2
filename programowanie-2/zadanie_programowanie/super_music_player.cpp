#include <string>
#include <algorithm>
#include <iostream>
#include <cstring>

#include "implementation/super_music_player.h"


void SuperMusicPlayer::start()
{
    this->isMusicOn = !this->isMusicOn;
}

void SuperMusicPlayer::stop()
{
    this->isMusicOn = !this->isMusicOn;
}

void SuperMusicPlayer::next()
{
    if (this->actualList.size() == 0)
        return;
    auto song = find(this->actualList.begin(), this->actualList.end(), this->Player::actualSong);
    if (song == actualList.end())
    {
        this->Player::actualSong = this->actualList[0];
    }
    else
    {
        this->Player::actualSong = *(song + 1);
    }
}

void SuperMusicPlayer::previous()
{
    auto song = find(this->actualList.begin(), this->actualList.end(), this->Player::actualSong);
    int index = song - this->actualList.begin() - 1;
    if (index != 0)
    {
        this->Player::actualSong = this->actualList[index - 1];
    }
    else
    {
        this->Player::actualSong = this->actualList[this->actualList.size() - 1];
    }
}

void SuperMusicPlayer::changeLoopStatus()
{
    this->isLoop = !this->isLoop;
}

bool SuperMusicPlayer::getLoopStatus()
{
    return this->isLoop;
}



void SuperMusicPlayer::setActualSong(SongBase *song)
{
    this->Player::actualSong = song;
}

void SuperMusicPlayer::setSongList(vector<SongBase *> songList)
{
    this->actualList = songList;
}

vector<SongBase *> SuperMusicPlayer::getSongList()
{
    return this->actualList;
}

int SuperMusicPlayer::getMinutes()
{
    return this->actualSongMinute;
}
int SuperMusicPlayer::getSeconds()
{
    return this->actualSongSecond;
}

void SuperMusicPlayer::setMinute(int minute)
{
    this->actualSongMinute = minute;
}
void SuperMusicPlayer::setSeconds(int second)
{
    this->actualSongSecond = second;
}

SongBase* SuperMusicPlayer::getActualSong() {
    //calling from class "A"
    return this->Player::getActualSong();
}
